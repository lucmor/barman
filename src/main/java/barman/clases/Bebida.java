package barman.clases;

abstract class Bebida {
	protected String nombre;
	protected int precio;

	public Bebida(String nombre, int precio) {
		this.nombre = nombre;
		this.precio = precio;
	}

	public String getNombre() {
		return this.nombre;
	}

	public int getPrecio() {
		return this.precio;
	}
}